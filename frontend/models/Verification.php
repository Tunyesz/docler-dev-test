<?php

namespace frontend\models;

use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Verify user by the given authorization key
 */
class Verification extends Model {

    /**
     * User's authorization key
     * @var string
     */
    public $authorization_key;

    /**
     * User model
     * @var User
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['authorization_key', 'validateAuthorizationKey'],
            ['authorization_key', 'validateUserStatus'],
        ];
    }
    
    public function verify() {
        $user = $this->getUser();
        $user->status = User::STATUS_ACTIVE;
        
        if($user->save()) {
            return $user;  
        }
        
        return false;
    }

    /**
     * Validate wether the user has been alerady verified
     * @param string $attribute
     * @param array|null $params
     * @return boolean
     */
    public function validateUserStatus($attribute, $params) {
        if (@$this->getUser()->status !== User::STATUS_VERIFICATION) {
            $this->addError($attribute, Yii::t('login', 'verification-already-verified'));
            return false;
        }

        return true;
    }

    /**
     * Validate wether the user exists with the current authorization key
     * @param type $attribute
     * @param type $params
     * @return boolean
     */
    public function validateAuthorizationKey($attribute, $params) {
        if (!$this->getUser()) {
            $this->addError($attribute, Yii::t('login', 'verification-invalid-auth-key'));
            return false;
        }

        return true;
    }

    /**
     * Return User that needs to be verified
     * @return User
     */
    protected function getUser() {
        if ($this->_user)
            return $this->_user;

        $this->_user = User::find()
                        ->where('auth_key=:authorizationKey', [':authorizationKey' => $this->authorization_key]
                        )->one();

        return $this->_user;
    }

}
