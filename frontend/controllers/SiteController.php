<?php
namespace frontend\controllers;

use common\models\LoginForm;
use frontend\models\SignupForm;
use frontend\models\Verification;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'greeting'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['greeting'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if($model->login()) {
                return $this->redirect(['site/greeting']);
            }else {
                $model->onFailedLogin();
            }
        }
        
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    /**
     * Verifies user by the url of the email
     * @return mixed
     */
    public function actionVerify() {
        $model = new Verification();
        $model->authorization_key = @$_GET['auth_key'];
        
        if($model->validate()) {
            if($user = $model->verify()) {
                if (Yii::$app->getUser()->login($user)) {
                    // Redirect to greeting page
                    return $this->redirect(['site/greeting']);
                }
            } else {
                Yii::$app->session->setFlash('error', \Yii::t('login', 'There was an error during the verification'));   
            }
        } else {
            Yii::$app->session->setFlash('error', \Yii::t('login', 'There was an error during the verification'));   
        }
        
        return $this->goHome();
    }

    /**
     * Displays greeting page.
     *
     * @return mixed
     */
    public function actionGreeting()
    {
        return $this->render('greeting');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if($model->sendVerificationEmail($user)) {
                    Yii::$app->session->setFlash('success', Yii::t('login', 'successfull-signup-alert-message'));
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
}
