<?php
return [
    'successfull-signup-alert-message' => 'Check your email to verify your account.',
    'verification-email-subject' => 'Sign up verification ',
    'verification-already-verified' => 'User has been already verified',
    'verification-invalid-auth-key' => 'Invalid authorization key',
    'verification-error' => 'There was an error during the verification',
];