<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    /**
     * Login username
     * @var string 
     */
    public $username;
    /**
     * Login password
     * @var string 
     */
    public $password;
    /**
     * Only log out the user if signed out
     * @var boolean 
     */
    public $rememberMe = true;
    /**
     * Verification code for capthca
     * @var string 
     */
    public $verifyCode;
    /**
     * User Active Record
     * @var User
     */
    private $_user;
    /**
     * Login limiters
     * @var login\LoginLimiterInterface[]
     */
    private $_limiters = [];
    
    /**
     * After getting the data set the limiters
     * 
     * @inheritdoc
     */
    public function load($data, $formName = null) {
        $load = parent::load($data, $formName);
        
        $this->_limiters['same-user'] = new login\LoginLimiter(
                'login.failed.same-user.',
                $this->username,
                Yii::$app->params['login.failed.same-user.max-number-of-tries'],
                Yii::$app->params['login.failed.ttl']
        );
        
        $this->_limiters['same-ip'] = new login\LoginLimiter(
                'login.failed.same-ip.',
                $this->getClientIP(),
                Yii::$app->params['login.failed.ip.max-number-of-tries'],
                Yii::$app->params['login.failed.ttl']
        );
        
        $this->_limiters['same-ip-masked-24'] = new login\LoginLimiter(
                'login.failed.same-ip-masked-24.',
                $this->getClientMaskedIP('255.255.255.0'),
                Yii::$app->params['login.failed.ip-masked-24bits.max-number-of-tries'],
                Yii::$app->params['login.failed.ttl']
        );

        $this->_limiters['same-ip-masked-16'] = new login\LoginLimiter(
                'login.failed.same-ip-masked-16.',
                $this->getClientMaskedIP('255.255.0.0'),
                Yii::$app->params['login.failed.ip-masked-24bits.max-number-of-tries'],
                Yii::$app->params['login.failed.ttl']
        );
        
        return $load;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // user must be verified
            [['username'], 'validateVerified'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha', 'when' => [$this, 'isCaptchaRequired']],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }
    /**
     * Validate whether the user is already verified
     * 
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateVerified($attribute, $params) {
        if(@$this->getUser()->status == User::STATUS_VERIFICATION) {
            $this->addError($attribute, 'User must be verified, check your email.');
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
    /**
     * On failed login icrease the limiters' counter
     * The counter of the IP and the user name is increased for every failure,
     * however the ranges are only increased when
     * there is no captcha displayed from any reason.
     */
    public function onFailedLogin() {
        // Key names of limiters need to be increased only if no captcha displayed
        $increaseOnlyOnNoCaptchaDisplayed = ['same-ip-masked-24', 'same-ip-masked-16'];
        
        foreach($this->getLimiters() as $name => $limiter) {
            if(!$this->isCaptchaRequired() && in_array($name, $increaseOnlyOnNoCaptchaDisplayed)) {
                continue;
            }
            $limiter->increase();
        }
    }
    
    /**
     * Is captcha required to be shown on login screen
     * @return boolean
     */
    public function isCaptchaRequired() {
        foreach($this->getLimiters() as $limiter) {
            // If one of the limiter is not valid then captcha is required
            if(!$limiter->isValid()) {
                return true;
            }
        }
        
        return false;
    }
    /**
     * Get all login limiters
     * @return login\LoginLimiterInterface[]
     */
    protected function getLimiters() {
        return $this->_limiters;
    }
    
    /**
     * Returns the client's IP address
     * @return string
     */
    protected function getClientIP() {
        return $_SERVER['REMOTE_ADDR'];
    }
    
    /**
     * Get back the client's masked IP address
     * @param string $_mask
     * @return string
     */
    public function getClientMaskedIP($mask) {
        return long2ip( ip2long($this->getClientIP()) & ip2long($mask) );
    }
}
