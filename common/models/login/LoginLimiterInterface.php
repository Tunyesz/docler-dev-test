<?php

namespace common\models\login;

/**
 * Limiter interface for login restrictions
 */
interface LoginLimiterInterface {
    /**
     * Determines whether the limiter exceeded its' limit
     */
    public function isValid();
    /**
     * Clears the counter
     */
    public function clear();
    /**
     * Increase the counter
     */
    public function increase();
    /**
     * Get the counter value
     */
    public function getCounter();
    /**
     * Get the limit of the counter
     */
    public function getLimit();
}
