<?php
namespace common\models\login;

use Yii;

/**
 * Limiter class for login form
 * Checks if the client has tried to many times
 * Stores the counter in cache
 */
class LoginLimiter implements LoginLimiterInterface {
    /**
     * Cache key prefix for storing
     */
    private $_cachePrefix;
    /**
     * Cache expire time in seconds
     */
    private $_cacheExpire;
    /**
     * Identifier to distinct entities
     * @var string 
     */
    private $_identifier;
    /**
     * Maximum amount of tries
     * @var int
     */
    private $_limit = 3;

    public function __construct($cachePrefix, $identifier, $limit, $expire = 3600) {
        $this->_cachePrefix = $cachePrefix;
        $this->_identifier = $identifier;
        $this->_limit = $limit;
        $this->_cacheExpire = $expire;
    }
    
    /**
     * @inheritdoc
     */
    public function isValid() {
        return ($this->getCounter() < $this->getLimit());
    }
    /**
     * Delete the counter
     * @return bool deletion is succeeded
     */
    public function clear() {
        return Yii::$app->cache->delete($this->getCachePrefix().$this->getIdentifier());
    }
    
    /**
     * Gets the number of failed attemption
     * @return int counter
     */
    public function getCounter() {
        // try retrieving counter from cache
        $counter = Yii::$app->cache->get($this->getCachePrefix().$this->getIdentifier());
        
        return ($counter)?$counter:0;
    }

    /**
     * Increases the counter of failed attemption
     * @return int counter
     */
    public function increase() {
        // try retrieving $data from cache
        $count = Yii::$app->cache->get($this->getCachePrefix().$this->getIdentifier());

        // $count is not found in cache
        if ($count === false) {           
            $count = 0;
        }
        
        // Store increased counter in cache
        Yii::$app->cache->set($this->getCachePrefix().$this->getIdentifier(), ++$count, $this->getCacheExpire());
        
        return $count; 
    }

    /**
     * Returns the trial's identifier
     * @return string
     */
    protected function getIdentifier() {
        return $this->_identifier;
    }
    
    /**
     * Cache key prefix for storing
     * @return string
     */
    protected function getCachePrefix() {
        return $this->_cachePrefix;
    }

    /**
     * Cache expire time in seconds
     * @return int
     */
    protected function getCacheExpire() {
        return $this->_cacheExpire;
    }
    
    /**
     * @inheritdoc
     */
    public function getLimit() {
        return $this->_limit;
    }
}