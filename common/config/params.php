<?php
return [
    'adminEmail' => 'docler-admin@example.com',
    'supportEmail' => 'docler-support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    
    // Failed logins
    'login.failed.ip.max-number-of-tries' => 3,
    'login.failed.ip-masked-24bits.max-number-of-tries' => 500,
    'login.failed.ip-masked-16bits.max-number-of-tries' => 1000,
    'login.failed.same-user.max-number-of-tries' => 3,
    'login.failed.ttl' => 3600, // Expire time of counters
];